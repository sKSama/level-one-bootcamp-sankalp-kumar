#include <stdio.h>

struct fract
{
    int num;
    int dem;
} ;


struct fract input()
{
struct fract x;

printf("Enter the numerator for number :" );
 scanf("%d",&x.num);


printf("Enter the denominator for number : ");
 scanf("%d",&x.dem);
    return x;
}

int numcalc(int a, int b, int c, int d)
{
    return ((a*d)+(b*c));
}

int demcalc(int b, int d)
{
    return (b*d);
}

int gcdcal(int x, int y)
{
    int gcd=1;
    for(int i=1; i <= x && i <= y; ++i)
    {
        if(x%i==0 && y%i==0)
            gcd = i;
    }
    return gcd;
}

int main()
{

struct fract n1,n2,ans;

int x,y,gcd;

printf("1st Number :\n" );
n1=input();

printf("2nd Number :\n" );
n2 =input();

x= numcalc(n1.num,n1.dem,n2.num,n2.dem); //numerator


y=demcalc(n1.dem,n2.dem); //denominator


gcd = gcdcal(x,y);


ans.num=x/gcd;
ans.dem=y/gcd;

printf("\nThe added fraction is %d/%d" ,ans.num,ans.dem);
printf("\n");
return 0;
}
