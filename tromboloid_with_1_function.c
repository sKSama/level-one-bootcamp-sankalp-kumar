//Write a program to find the volume of a tromboloid using one function
//2nd commit
#include <stdio.h>

float volumeofT(int, int,int);

int main()
{
    int h,d,b;
    float vol;
    printf("Enter the h : ");
    scanf("%d", &h);

    printf("Enter the b : ");
    scanf("%d", &b);
    
    printf("Enter the d : ");
    scanf("%d", &d);

    vol = volumeofT(h,b,d);

    printf("Volume : %f", vol);

    return 0;
}

float volumeofT(int h, int b,int d)
{
    return (1/3.0 * ((h * d) + d) / b*1.0);
}