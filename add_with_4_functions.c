#include <stdio.h>

int inputa()
{
    int x;
    printf("Enter 1st Number: ");
    scanf("%d",&x);
    return x;
}

int inputb()
{
    int x;
    printf("Enter 2nd Number: ");
    scanf("%d",&x);
    return x;
}

int add(int a, int b)
{
    return (a+b);
}


void print (int sum)
{
    printf("Sum = %d\n",sum);
}

int main()
{
    int a=inputa();
    int b=inputb();
    int sum=add(a,b);
    print(sum);

    return 0;
}
